<?php
    $ages = [
        "Alice" => 34,
        "Bob"   => 29,
        "Carol" => 24
    ];

    $x = array_values($ages);
    $avgAge = array_sum($x) / count($x);
?>

<html>
    <p>Average age: <?= $avgAge ?> </p>
</html>
