<?php

class Camera
{
    private $make;
    private $model;
    private $megaPixels;

    public function __construct($make, $model, $megaPixels)
    {
        $this->make = $make;
        $this->model = $model;
        $this->megaPixels = $megaPixels;
    }

    public function getMegapixels()
    {
        return $this->megaPixels;
    }

    public function getMake()
    {
        return $this->make;
    }

    public function getModel()
    {
        return $this->model;
    }
}

$camera = new Camera('Canon', 'G7X', 1);

?>

<h1>For sale: <?= $camera->getMake() ?> <?= $camera->getModel() ?></h1>
<p>MPx: <?= $camera->getMegapixels() ?></p>
