<?php

class Queue
{
    private $people;

    public function __construct($people)
    {
        $this->people = $people;
    }

    public function removePerson($name)
    {
        $this->people = array_diff($this->people, [$name]);
    }

    public function getPeople()
    {
        return $this->people;
    }

    public function getQueueLength()
    {
        return count($this->people);
    }

    public function getPerson($key)
    {
        return $this->people[$key];
    }
}

