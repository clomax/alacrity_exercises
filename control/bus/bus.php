<?php

class Bus
{
    private $capacity;
    private $passengers;

    public function __construct($capacity, $passengers)
    {
        $this->capacity = $capacity;
        $this->passengers = $passengers;
    }

    public function addPassenger($name)
    {
        $added = false;
        if($this->getPassengerCount() < $this->getCapacity())
        {
            $this->passengers[] = $name;
            $added = true;
        }
        return $added;
    }

    public function getSpacesLeft()
    {
        return ($this->capacity - count($this->passengers));
    }

    public function getCapacity()
    {
        return $this->capacity;
    }

    public function getPassengers()
    {
        return $this->passengers;
    }

    public function getPassengerCount()
    {
        return count($this->passengers);
    }

    public function canAddPassenger()
    {
        return count($this->passengers) < $this->capacity;
    }
}

