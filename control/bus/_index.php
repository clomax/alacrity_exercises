<!DOCTYPE html>

<?php
    require_once('bus.php');
    require_once('queue.php');

    function printBusPassengers($b)
    {
        $passengers = $b->getPassengers();
        ?>
            <h4>Bus Passengers (<?= count($passengers)?>):</h4>
            <ul>
                <?php
                    foreach($passengers as $p)
                    {
                        ?>
                            <li><?= $p ?></li>
                        <?php
                    }
                ?>
            </ul>
        <?php
    }

    function printQueue($q)
    {
        $people = $q->getPeople();
        ?>
            <h4>Queue (<?= count($people) ?>):</h4>
            <ul>
                <?php
                    foreach($people as $p)
                    {
                        ?>
                            <li><?= $p ?></li>
                        <?php
                    }
                ?>
            </ul>
        <?php
    }

    $bus = new Bus(10, ['alice', 'bob', 'carol', 'dave', 'eric', 'fred', 'george']);
    $queue = new Queue(['henry', 'inge', 'james', 'karl', 'leif', 'mike', 'nadia', 'oscar', 'paul']);
?>



<html>
    <body>
        <p>Bus Capacity: <?= $bus->getCapacity() ?></p>

        <?php
            printBusPassengers($bus);
            printQueue($queue);
        ?>

        <hr>

        <p>
            Putting
                <?php
                    for ($i = 0 ; $i < $queue->getQueueLength() ; $i++)
                    {
                        $person = $queue->getPerson($i);
                        ?>
                            <i><?= $person ?>,</i>
                        <?php
                            if ($bus->canAddPassenger())
                            {
                                $bus->addPassenger($person);
                                $queue->removePerson($person);
                            }
                    }
                ?>
            on the bus...
        </p>

        <?php
            printBusPassengers($bus);
            printQueue($queue);
        ?>
    </body>
</html>

