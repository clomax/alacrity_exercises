<?php require_once('head.php') ?>

<div class="container">
    <h4>Prime number test:</h4>
    <form action="prime.php">
	<input type="number" name="number">
	<button type="submit">Sumbit</button>
    </form>

    <h4>Primes within range:</h4>
    <form action="prime_range.php">
	<input type="number" name="min">
	<input type="number" name="max">
	<button type="submit">Sumbit</button>
    </form>
</div>
