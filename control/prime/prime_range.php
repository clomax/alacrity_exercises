<?php

    require_once('prime.php');

    function primes_in_range($min, $max)
    {
        $fs = [];
        $i = $min;
        while($i < $max)
        {
            $res = is_prime($i);
            if($res[0])
                $fs[] = $i;
            $i++;
        }
        return $fs;
    }

    $min = $_GET['min'];
    $max = $_GET['max'];
    $prime_numbers = primes_in_range($min, $max);
?>

<?php require_once('head.php') ?>

<div class="container">
    <h3>Primes in range <?= $min ?> : <?= $max ?></h3>

    <?php
    foreach($prime_numbers as $n)
    {
        ?>
            <li><?= $n ?></li>
        <?php
    }
?>

</div>
