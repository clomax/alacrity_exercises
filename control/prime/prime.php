<?php
    function factors($n)
    {
        $fs = [];
        $i = 1;
        while($i <= $n)
        {
            if($n % $i == 0)
                $fs[] = $i;

            $i++;
        }
        return $fs;
    }

    function is_prime($n)
    {
        $prime = false;
        $fs = factors($n);
        if (count($fs) == 2)
        {
            $prime = true;
        }
        return array($prime, $fs);
    }


    $number = $_GET['number'];
    $result = is_prime($number);
    $s_numberIsPrime = ($result[0]
			? $number . ' is prime'
			: $number . ' is not prime');
    $a_numberFactors = $result[1];
?>

<?php require_once('head.php') ?>

<body>
    <div class="container">
        <?php
            if($number)
            {
        ?>
            <h3>
                <?= $s_numberIsPrime ?>
            </h3>

	<br>
	<h4>Factors</h4>
        <?php
            }
        ?>
	<?php
	    foreach($a_numberFactors as $f)
	    {
		?>
		    <li><?= $f ?></li>
		<?php
	    }
	?>
    </div>
</body>
