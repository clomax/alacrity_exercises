<?php
    $num = 100;
    $sum = 0;

    for ($i=0; $i<100; $i++)
        $sum += $i;
?>

<html>
    <body>
        <h1>Hello</h1>

        <p>PHP is saying hello:</p>
        <p>
            <?php
                //print seems to do the same thing as echo
                print("Hello!");
            ?>
        </p>

        <br>

        <p>PHP has calculated the sum of numbers under 100:</p>
        <?= $sum ?>
    </body>
</html>
