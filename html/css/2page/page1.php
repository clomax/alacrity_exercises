<!DOCTYPE html>
<html lang="en">
<?php require_once('include/head.php'); ?>
<body>
    <?php require_once('include/header.php'); ?>

    <div class="container">
        <?php require_once('include/sidebar.php'); ?>

        <div class="content">
            <h1>Page 1</h1>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis velit dictum, dignissim metus eget, ultricies dui. Integer id lorem sit amet risus consequat lacinia non a tortor. Praesent egestas augue sed pellentesque tincidunt. Vivamus posuere dolor libero, id gravida nulla dapibus dignissim. Mauris ligula orci, fringilla vitae ornare fermentum, placerat eget diam. Morbi bibendum aliquam leo at molestie. Nunc eu tortor facilisis, fermentum mauris vel, scelerisque tortor. Sed euismod vitae felis in aliquam. Duis blandit ligula leo, ac consectetur odio porta sit amet. Sed a feugiat purus. Pellentesque sit amet feugiat ante. Praesent et ipsum ut metus cursus vestibulum. Vestibulum quis pellentesque magna, eu aliquet turpis. Pellentesque tincidunt diam elit, a egestas dui lacinia sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            <p>Aliquam auctor tincidunt nulla ut lobortis. Fusce sit amet sagittis nisi. Nam quis eros in massa posuere faucibus pharetra vel orci. Aliquam pulvinar velit dolor, nec auctor lectus posuere vitae. Mauris ac congue lectus. Mauris egestas finibus aliquam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse vulputate ligula purus, at malesuada leo tempor nec. Praesent auctor rutrum risus ac vestibulum. Mauris laoreet sem felis, sed aliquet ligula pellentesque id. Morbi ac elementum risus. </p>
        </div>
    </div>

    <?php require_once('include/footer.php'); ?>
</body>
</html>
