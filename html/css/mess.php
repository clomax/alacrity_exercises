<?php
    function randomPosition()
    {
        return rand(0,9000);
    }

    function randomCol()
    {
        return rand(0,255);
    }

    $randomColours = [];
    for($i=0; $i<2000; $i++)
        $randomColours[] = randomCol();

    $els = [
        'p',
        'img',
        'button',
        'a',
        'strong',
        'h1',
        'h2',
        'h3',
    ];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <?php
        for($i=0; $i<1000; $i++)
        {
            ?>
                <div class="rand-pos" style="width: 200px; height: 200px; position: absolute; left: <?= rand(0,100) ?>px; top: <?= rand(0,100) ?>px; background-color: rgb(<?= $randomColours[rand(0,2000)] ?>,<?= $randomColours[rand(0,2000)] ?>,<?= $randomColours[rand(0,2000)] ?>); z-index: -1000"></div>
                <<?= $els[array_rand($els)] ?> style="position: absolute; left: <?= rand(0,100) ?>px; top: <?= rand(0,100) ?>px;">
                    HELLO!
                <<?= "/" . $els[array_rand($els)] ?> >
            <?php
        }
    ?>
</body>
</html>
