<?php
session_start();
$counter = (isset($_SESSION['counter']))
    ? ++$_SESSION['counter']
    : 1;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <h1>You have been here: <?= $_SESSION['counter'] ?> times!</h1>
</body>
</html>
