<?php

require_once('database.php');
require_once('app/models/Post.php');

session_start();

$user_id = $_SESSION['user']['user_id'];
$title = trim($_POST['title'], " ");
$body = trim($_POST['body'], " ");
$price = $_POST['price'];

$post = new Post($user_id, $title, $body, $price);
$post->save();
header('Location: index.php');

