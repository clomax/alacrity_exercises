<?php

class DBConn
{
    private $server;
    private $user;
    private $password;
    private $db;
    private static $_DB_CONN;

    protected function __construct($server, $user, $password, $db)
    {
        $this->server   = $server;
        $this->user     = $user;
        $this->password = $password;
        $this->db       = $db;
    }

    public static function getInstance()
    {
        $DB_CONN = new mysqli(
            'localhost',
            'twitbay',
            'twitbay_',
            'twitbay'
        );

        if($DB_CONN->connect_errno)
            echo("Failed to connect to database: ("
                 . $DB_CONN->connect_errno . ") "
                 . $DB_CONN->connect_error
        );

        return $DB_CONN;
    }

    public function close()
    {
        $DB_CONN->close();
    }

}
