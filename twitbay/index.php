<?php
    session_start();
    require_once('database.php');
    require_once('app/models/User.php');
    require_once('app/models/Post.php');
?>

<!DOCTYPE HTML>
<html class=" js">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Twitbay</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<link href="http://twitter.com/phoenix/iefavicon.ico" rel="shortcut icon" type="image/x-icon">
		<link rel="stylesheet" href="twitbay_files/phoenix_core_logged_out.css" type="text/css" media="screen">
		<style charset="utf-8" class="lazyload">
			@import "twitbay_files/phoenix_more.bundle.css";
		</style>

        <?php
            $randomBackground = "twitbay_files/background" . rand(1,5) . ".jpg";
        ?>
		<style id="user-style-Hicksdesign-bg-img">
            body.user-style-Hicksdesign {
                background-image: url(<?=$randomBackground?>);
            }
		</style>
	</head>

	<body class="logged-out  mozilla user-style-Hicksdesign">
		<div class="route-profile" id="doc">
        	<div id="top-stuff">
          	<div id="top-bar-outer">
          		<div id="top-bar-bg"></div>
                <div id="top-bar">
                <div style="left: 0px;" class="top-bar-inside">
                	<div class="static-links">
    					<div id="logo">
        					<a href="http://localhost/bootcamp/twitbay">
        						<img src="twitbay_files/logo.png" alt="nope"/>
        					</a>
    					</div>
    				</div>
    				<div class="active-links">
        				<div id="session">
                            <?php if(! isset($_SESSION['user'])) { ?>
        						<form action="login.php" method="POST">
        							Username: <input type="text"     name="username" />
        							Password: <input type="password" name="password" />
        							<input type="submit" value="Sign in" />
        						</form>
                            <?php } else {
                                $username = $_SESSION['user']['username'];
                            ?>
                                <p>Welcome back to TwitBay, <?= $_SESSION['user']['username'] ?></p>
                                <form action="logout.php" method="POST">
                                    <button>Log out</button>
                                </form>
                            <?php } ?>
        				</div>
    				</div>
                </div>
              </div>
            </div>
        </div>
        <div id="page-outer">
          	<div class="profile-container" id="page-container">
          		<div>
          			<div style="min-height: 683px;" class="main-content">
    					<div class="profile-header">
    							<div class="profile-info clearfix">
       	 							<div class="profile-image-container">
          							   <img src="twitbay_files/megaphone.png" alt="Twitbay">
        							</div>
        							<div class="profile-details">
                                        <div class="full-name">
      										<h2>Twitbay Listings</h2>
      									</div>
          						    <div class="screen-name-and-location">
          							   <span class="screen-name screen-name-Hicksdesign pill">@Twitbay</span>
          							   Cardiff, Wales, UK
          						    </div>
          						    <div class="bio">
          							   Only the finest products, services, bric-a-brac and budget bargains - all served
              						   with a dash of ASP.NET goodness...
              						</div>
      						        <div class="url">
                                        <a target="_blank" rel="me nofollow" href="http://www.spiderstudios.co.uk/">http://www.spiderstudios.co.uk/</a>
      						        </div>
                                </div>
  						</div>
    					<ul class="stream-tabs">
                            <li class="stream-tab stream-tab-tweets active">
                               <a href="index.php" class="tab-text">All posts</a>
                            </li>

                            <?php
                                if(isset($_SESSION['user']))
                                {
                                    $href="?u=" . $_SESSION['user']['user_id'];
                            ?>
                                    <li class="stream-tab stream-tab-tweets active">
                                    <a href=<?= $href ?> class="tab-text">My posts</a>
                                    </li>
                            <?php
                                }
                            ?>
					   </ul>
                    </div>
    				<div class="stream-manager">
  						<div class="js-stream-manager" id="profile-stream-manager">
  							<div class="stream-container">
  								<div class="stream stream-user">
  									<div id="stream-items-id" class="stream-items">

                                        <?php
                                            $posts = (isset($_GET['u']))
                                                ? Post::byUserID($_SESSION['user']['user_id'])
                                                : Post::all();
                                            foreach($posts as $p)
                                            {
                                        ?>
                                            <div media="true" class="js-stream-item stream-item">
                                                <div class="stream-item-content tweet js-actionable-tweet stream-tweet">
                                                    <div class="tweet-image">
                                                        <img src=<?= $p[1] ?> alt="Post pic" class="user-profile-link" height="48" width="48">
                                                    </div>
                                                    <div class="tweet-content">
                                                    <a class="tweet-screen-name user-profile-link"><?= $p[0] ?></a>
                                                    <i><?= $p[2] ?></i>
                                                    <p class="tweet-text js-tweet-text"><?= $p[3] ?></p>
                                                    <p class="tweet-timestamp"><?= $p[4] ?>   &pound;<?= $p[5] ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                            }
                                        ?>

										<!-- end of item -->

									</div>
								</div>
							</div>
						</div>
					</div>
  				</div>
				<div class="dashboard profile-dashboard">
          			<div class="component">
          				<div class="signup-call-out">
          					<h1>
          						<span>Twitbay Users</span>
          					</h1>
        						<h2>Don't miss products from your favourite users!</h2>

                                <?php
                                    $usernames = User::allUsernames();
                                    foreach($usernames as $u)
                                    {
                                ?>
                                        <li><?= $u[0] ?></li>
                                <?php
                                    }
                                ?>

        						<hr />

        						<div class="profile-subpage-call-out">

                                    <?php
                                        if(isset($_SESSION['user']))
                                        {
                                            ?>
                                                <form action="new_post.php" method="POST">
                                                    <input name="title" placeholder="title">
                                                    <textarea rows="4" cols="30" name="body" placeholder="What's on your mind?..."></textarea>
                                                    <input type="number" name="price" value="0">
                                                    <button type="sumbit">Submit</button>
                                                </form>
                                            <?php
                                        }
                                    ?>


                                    <?php
                                        if (!isset($_SESSION['user']))
                                        {
                                    ?>
                                        <h5>Sign up</h5>
                                        <form action="signup.php" method="POST">
                                            Username: <input type="text"     name="username" />
                                            Password: <input type="password" name="password" />
                                            <input type="submit" value="Sign up" />
                                        </form>
                                    <?php
                                        }
                                    ?>

        							<hr />

        						</div>
        					</div>
        					<hr class="component-spacer">
        				</div>
        				<div class="component">
        					<div class="dashboard-profile-annotations clearfix">
        						<h2 class="dashboard-profile-title">
        							<img src="twitbay_files/megaphone.png" alt="Acme Rocket Bikes" class="profile-dashboard" width="24">
        								Twitbay Stats
        						</h2>
      						</div>
                                <?php
                                    $userCount = User::count();
                                    $postCount = Post::count();
                                    $postSales = Post::totalSales();
                                ?>
    							<ul class="user-stats clearfix">
            						<li>
                                    <a class="user-stats-count"><?= $userCount ?><span class="user-stats-stat">Users</span></a>
            						</li>
            						<li>
            							<a class="user-stats-count"><?= $postCount ?><span class="user-stats-stat">Posts</span></a>
            						</li>
    								<li>
                                    <a class="user-stats-count">&pound;<?= $postSales ?><span class="user-stats-stat">Sales</span></a>
    								</li>
    							</ul>

    							<hr class="component-spacer">
    						</div>
      				</div>
				</div>
				<hr class="component-spacer">
			</div>
		</div>
		<div class="component">
			<hr class="component-spacer">
		</div>
		<!--[if lte IE 6]>
  		<script>using('bundle/ie6').load();</script>
		<![endif]-->
	</body>
</html>

