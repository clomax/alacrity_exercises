<?php

require_once('database.php');

$db_conn = DBConn::getInstance();

$username = $_POST['username'];
$password = $_POST['password'];

$query = $db_conn->prepare("
          SELECT id, password
          FROM users WHERE
          username = ?");
$query->bind_param("s", $username);
if($query->execute())
{
    $result = $query->get_result()->fetch_all()[0];
    $user_id = $result[0];
    $db_pass = $result[1];

    if(password_verify($password, $db_pass))
    {
        session_destroy();
        session_start();
        $_SESSION['user']['user_id'] = $user_id;
        $_SESSION['user']['username'] = $username;
    }
}

header('Location: index.php');

