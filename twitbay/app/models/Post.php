<?php

class Post
{
    private $user_id;
    private $title;
    private $body;
    private $price;

    public function __construct($user_id, $title, $body, $price)
    {
        $this->user_id  = $user_id;
        $this->title    = $title;
        $this->body     = $body;
        $this->price    = $price;
    }

    public function save()
    {
        $db_conn = DBConn::getInstance();
        $query = $db_conn->prepare(
            "INSERT INTO posts (user_id, title, body, price)
             VALUES (?, ?, ?, ?)");
        $query->bind_param("issi",
                $this->user_id, $this->title, $this->body, $this->price);
        $result = $query->execute();
    }

    public static function all()
    {
        $db_conn = DBConn::getInstance();
        $query = "
            SELECT users.username, users.image, title, body, datetime, price
            FROM posts
            JOIN users ON users.id = posts.user_id
            ORDER BY datetime DESC";

        $posts = false;
        if($result = $db_conn->query($query))
            $posts = $result->fetch_all();

        return $posts;
    }

    public static function byUserID($user_id)
    {
        $db_conn = DBConn::getInstance();
        $query = $db_conn->prepare("
            SELECT users.username, users.image, title, body, datetime, price
            FROM posts
            JOIN users ON users.id = posts.user_id
            WHERE users.id = ?
            ORDER BY datetime DESC");

        $query->bind_param("s", $user_id);

        $posts = false;
        if($query->execute())
            $posts = $query->get_result()->fetch_all();

        return $posts;
    }

    public static function count()
    {
        $db_conn = DBConn::getInstance();
        $postCountQuery = "SELECT COUNT(*) FROM posts";
        $postCount = $db_conn->query($postCountQuery)->fetch_array()[0];
        return $postCount;
    }

    public static function totalSales()
    {
        $db_conn = DBConn::getInstance();
        $postSalesQuery = "SELECT SUM(price) FROM posts";
        $postSales = $db_conn->query($postSalesQuery)->fetch_array()[0];
        return $postSales;
    }
}



