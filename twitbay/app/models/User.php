<?php
require_once ('database.php');

class User
{
    private $username;
    private $password;
    private $image;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    public function save()
    {
        $db_conn = DBConn::getInstance();
        $query = $db_conn->prepare(
                 "INSERT INTO users
                  (username, password)
                  VALUES
                  (?, ?)");
        $query->bind_param("ss", $this->username, $this->password);
        $result = $query->execute();
    }

    public static function count()
    {
        $db_conn = DBConn::getInstance();
        $userCountQuery = "SELECT COUNT(*) FROM users";
        $userCount = $db_conn->query($userCountQuery)->fetch_array()[0];
        return $userCount;
    }

    public static function allUsernames()
    {
        $db_conn = DBConn::getInstance();
        $usernames = false;
        $query = "
            SELECT username
            FROM users
            ORDER BY id DESC";
        if($result = $db_conn->query($query))
            $usernames = $result->fetch_all();

        return $usernames;
    }
}

